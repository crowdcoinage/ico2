var video;
function onYouTubeIframeAPIReady() {
    video = new YT.Player('video', {
        videoId: 'UZcfVPUW7ig',
        playerVars: {
            color: 'white',
            rel: 0,
            showinfo: 0
        },
    });
}

$(document.body).on('click', '#play__video', function(evt) {
    var el = $(this);
    el.fadeOut(200);
    setTimeout( function() {
        video.playVideo();
    }, 200);
});
