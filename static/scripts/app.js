jQuery(function($) {

    // Header add bg
    function header_scroll_top(el) {
        if (el.scrollTop() > 1) {
            $('.header').removeClass('js__open')
            $('.header.header--with-bg').addClass('js__open')
        } else {
            $('.header').removeClass('js__open')
            $('.header:not(.header--with-bg)').addClass('js__open')
        }
    }

    $(window).on('scroll', function() {
        var el = $(this)
        header_scroll_top(el)
    })
    header_scroll_top($(window))

    // GA tracking
    $(document.body).on('click', '.button--ga', function() {
        var el = $(this)
        ga('send', 'event', el.attr('data-cat'), el.attr('data-action'), 'ICO Campaign')
    })
    // $(document.body).on('click', '.button--ga', function() {
    //     var el = $(this);
    //     var cat = el.attr('data-cat');
    //     var action = el.attr('data-action');
    //     var label = el.attr('data-label');
    //     ga('send', 'event', cat, action, label);
    // });

    // Scroll to element
    $(document.body).on('click', 'a[href*="#"]', function(event) {
        event.preventDefault()
        var el     = $(this)
        var id     = el.attr('href').split('#')[1]
        var offset = $('.header.header--with-bg').innerHeight() - 2

        if ($('[id="'+id+'"]').length) {
            if (el.closest('ul').hasClass('overlay__nav')) {
                $('.js__hide-menu').click()
            }
            $('html, body').stop(true, false).animate({
                scrollTop: $('[id="'+id+'"]').offset().top - offset
            }, 700)
        } else {
            window.location = window.location.origin + '#' + id
        }
    })

    // Active by section scroll
    function active_section_pos(cur_pos) {
        var cur_pos       = $(window).scrollTop()
        var section       = $('.section--scroll')
        var header_height = $('.header').innerHeight()

        section.each(function() {
            var el = $(this)
            var top = $(this).offset().top - header_height
            var bottom = top + $(this).innerHeight()
            if (cur_pos >= top && cur_pos <= bottom) {
                section.removeClass('active')
                el.addClass('active')
                var active_section = el.attr('id')
                if (active_section !== undefined) {
                    $('.primary__nav > li').removeClass('active')
                    $('.primary__nav > li > a[href="/#'+active_section+'"]').parent().addClass('active')
                } else {
                    $('.primary__nav > li').removeClass('active')
                }
            }
        })
    }
    active_section_pos()

    $(window).on('scroll', function() {
        var cur_pos = $(this).scrollTop()
        active_section_pos(cur_pos)
    })

    // Accordion
    $(document.body).on('click', '.accordion__item', function() {
        var target      = $(this)
        var target_hash = target.attr('data-hash')
        var $wrap       = target.closest('.accordion__wrap')

        $wrap.find('.accordion__item').removeClass('current')
        $wrap.find('.accordion__item dd').stop(true, false).slideUp(200).removeClass('current')

        target.addClass('current')
        target.find('dd').stop(true, false).slideDown(200).addClass('current')
    })

    // Modal
    $(document.body).on('click', '.card__link', function(event) {
        event.preventDefault()
        var el       = $(this)
        var el_modal = el.attr('data-modal')

        if (el_modal && $('.modal__wrap[data-modal="'+el_modal+'"]').length) {
            setTimeout(function() {
                $(document.body).addClass('disabled__scroll')
            }, 450)
            $('.modal__backdrop').addClass('js__open')
            $('.modal__wrap[data-modal="'+el_modal+'"]').addClass('js__open')

            setTimeout(function() {
                $(window).trigger('resize') // Trigger the resize event for the perfectScrollbar
                var selector = $('[data-modal="'+el_modal+'"]').find('.custom__scrollbar')
                var ps = new PerfectScrollbar(selector[0])
                ps.update()
            }, 300)
        }
    })
    $(document.body).on('click', '.close__modal, .modal__backdrop', function() {
        if ($('[data-modal*="person-"]').hasClass('js__open') || $('[data-modal*="advisor-"]').hasClass('js__open')) {
            $('.modal__backdrop').removeClass('js__open')
            $('.modal__wrap').removeClass('js__open')

            setTimeout(function() {
                $(document.body).removeClass('disabled__scroll')
            }, 450)
        }
    })
    $(document).on('keyup', function(event) {
        if (event.keyCode == 27) {
            $('.close__modal').click()
        }
    })

    // Add talent modal
    $('.open__join').on('click', function(event) {
        setTimeout(function() {
            $(window).trigger('resize') // Trigger the resize event for the perfectScrollbar
            var selector = $('#modalAfterSubmit').find('.custom__scrollbar')
            var ps = new PerfectScrollbar(selector[0])
            ps.update()
        }, 300)
    })

    // Mobile menu logic
    $('.js__show-menu').on('click', function() {
        $('.mobile__overlay').addClass('js__open')
        setTimeout(function() {
            $(document.body).addClass('disabled__scroll')
        }, 250)
    })
    $('.js__hide-menu').on('click', function() {
        $('.mobile__overlay').removeClass('js__open')
        setTimeout(function() {
            $(document.body).removeClass('disabled__scroll')
        }, 250)
    })

    // Detect browsers
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
    var is_iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream
    if (is_safari) {
        $(document.body).addClass('browser__safari')
    } else if (is_iOS) {
        $(document.body).addClass('browser__ios')
    }

    if ($('.custom__scrollbar').length) {
        var ps = new PerfectScrollbar('.custom__scrollbar')
    }
})

window.onload = function () {
    $('.accordion').find('dd.current').slideDown(200)

    var hash  = window.location.hash
    var split = hash.split('#')[1]

    if (hash) {
        setTimeout(function() {
            $('a[href="/#'+split+'"]').click()
            $('[data-modal="'+split+'"]').click()
            if (split.indexOf('tab-') === 0) {
                var closest_section_id = $('.accordion').closest('.section--scroll').attr('id')
                if (closest_section_id) {
                    $('a[href="/#'+closest_section_id+'"]').click()
                    if ($('.accordion__item[data-hash="'+split.split('tab-')[1]+'"]').length) {
                        $('.accordion__item[data-hash="'+split.split('tab-')[1]+'"]').click()
                    }
                }
            }
        }, 200)
    }

    // function progress_bar_calc() {
    //     var width = $('.progress__bar-item').first().innerWidth()
    //     $('.progress').css({
    //         'max-width': 'calc('+width+'px - 8px)'
    //     })
    // }
    // progress_bar_calc()
    // $(window).resize(progress_bar_calc)

    if ($('.custom__scrollbar').length) {
        var ps = new PerfectScrollbar('.custom__scrollbar')

        function modal_inner_height() {
            $.each($('.modal__content'), function(i, item) {
                $(item).removeAttr('style')
            })
            var modal_inner_height   = $('.js__open .modal__inner').height()
            var modal_content_height = $('.js__open .modal__content').height()
            if (modal_inner_height < modal_content_height) {
                $('.modal__inner').addClass('modal__inner--of')
            } else {
                $('.modal__inner').removeClass('modal__inner--of')
            }
            $('.modal__content').css({
                'height': modal_content_height
            })
            ps.update()
        }
        modal_inner_height()
        $(window).resize(modal_inner_height)
    }
}
