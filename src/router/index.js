import Vue from 'vue'
import Router from 'vue-router'
import Root from '@/components/Root'
import Page from '@/components/Page'
import Confirm from '@/components/Confirm'
import Unsubscribe from '@/components/Unsubscribe'
// import LeaderBoard from '@/components/LeaderBoard'
// import CrowdSale from '@/components/CrowdSale'
// import ReferralSignup from '@/components/ReferralSignup'

function readCookie (name) {
  var nameEQ = name + '='
  var ca = document.cookie.split(';')
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length)
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length)
    }
  }
  return null
}

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [

    // Lang routes
    {
      path: '/:lang(en|ru|kr|hi|ch|jp|es|pt|vn)',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '/',
          name: 'Lang_Root',
          component: Root
        },
        {
          path: 'privacy-policy',
          name: 'Lang_Privacy',
          component: Page,
          props: { pageId: 292 }
        },
        {
          path: 'terms-and-conditions',
          name: 'Lang_ToS',
          component: Page,
          props: { pageId: 294 }
        },
        // {
        //   path: 'leaderboard',
        //   name: 'Lang_Leaderboard',
        //   component: LeaderBoard
        // },
        {
          path: 'confirm/:token',
          name: 'Lang_Confirm',
          component: Confirm
        },
        {
          path: 'unsubscribe/:token',
          name: 'Lang_Unsubscribe',
          component: Unsubscribe
        },
        // {
        //   path: 'crowdsale/:nonce?',
        //   name: 'Lang_Crowdsale',
        //   component: CrowdSale
        // },
        // {
        //   path: 'airdrop-signup',
        //   name: 'Lang_ReferralSignup',
        //   component: ReferralSignup
        // },
        {
          path: 'kyc-policy',
          name: 'Lang_Kyc',
          component: Page,
          props: { pageId: 517 }
        }
      ]
    },

    // Langless routes
    {
      path: '/',
      name: 'Root',
      component: Root
    },
    {
      path: '/privacy-policy',
      name: 'Privacy',
      component: Page,
      props: { pageId: 292 }
    },
    {
      path: '/kyc-policy',
      name: 'Kyc',
      component: Page,
      props: { pageId: 517 }
    },
    {
      path: '/terms-and-conditions',
      name: 'ToS',
      component: Page,
      props: { pageId: 294 }
    },
    // {
    //   path: '/leaderboard',
    //   name: 'Leaderboard',
    //   component: LeaderBoard
    // },
    // {
    //   path: '/crowdsale/:nonce?',
    //   name: 'Crowdsale',
    //   component: CrowdSale
    // },
    {
      path: '/confirm/:token',
      name: 'Confirm',
      component: Confirm
    },
    // {
    //   path: '/airdrop-signup',
    //   name: 'ReferralSignup',
    //   component: ReferralSignup
    // },
    {
      path: '/unsubscribe/:token',
      name: 'Unsubscribe',
      component: Unsubscribe
    }
  ]
})

router.beforeEach((to, from, next) => {
  function isLang () {
    return window.language && window.language !== 'en'
  }

  function isLangRoute () {
    return to.name && to.name.indexOf('Lang_') !== -1
  }

  if (to.name) {
    window.pageName = to.name.replace('Lang_', '').toLowerCase()
  }

  // If first time, set lang
  if (!window.language) {
    window.language = 'en'
    if (readCookie('lang')) {
      window.language = readCookie('lang')
    }
  }

  // If param, set lang
  if (to.params.lang) {
    window.language = to.params.lang
  }

  // Add route lang if needed
  if (isLang() && !isLangRoute()) {
    next(`/${window.language}${to.path}`)
    return
  }

  window.scrollTo(0, 0)
  next()
})

export default router
