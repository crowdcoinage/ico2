export var langMap = {
  'en': {
    mainMenuId: 2,
    root: 67,
    unsubscribe: 67,
    confirm: 67,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 67,
    referralsignup: 67
  },
  'ru': {
    mainMenuId: 3,
    root: 533,
    unsubscribe: 533,
    confirm: 533,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 533,
    referralsignup: 67
  },
  'ch': {
    mainMenuId: 4,
    root: 541,
    unsubscribe: 541,
    confirm: 541,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 541,
    referralsignup: 67
  },
  'jp': {
    mainMenuId: 5,
    root: 822,
    unsubscribe: 822,
    confirm: 822,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 822,
    referralsignup: 67
  },
  'es': {
    mainMenuId: 6,
    root: 832,
    unsubscribe: 832,
    confirm: 832,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 832,
    referralsignup: 67
  },
  'pt': {
    mainMenuId: 7,
    root: 842,
    unsubscribe: 842,
    confirm: 842,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 842,
    referralsignup: 67
  },
  'vn': {
    mainMenuId: 8,
    root: 850,
    unsubscribe: 850,
    confirm: 850,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 850,
    referralsignup: 67
  },
  'hi': {
    mainMenuId: 9,
    root: 859,
    unsubscribe: 859,
    confirm: 859,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 859,
    referralsignup: 67
  },
  'kr': {
    mainMenuId: 10,
    root: 871,
    unsubscribe: 871,
    confirm: 871,
    kyc: 517,
    privacy: 292,
    tos: 294,
    crowdsale: 871,
    referralsignup: 67
  }
}
