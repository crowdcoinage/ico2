var APIversions = {
  live: 'https://crowdcoinage.com/api',
  dev: 'http://localhost:4567'
}

let URL = APIversions.live
let pageDataBaseURL = '/data/pages/'
let menuDataBaseURL = '/data/menus/'
if (window.location.hostname === 'localhost') {
  URL = APIversions.dev
  pageDataBaseURL = '/static/data/pages/'
  menuDataBaseURL = '/static/data/menus/'
}

export var configuration = {
  ICOStartDate: 'Feb 12, 2018 11:00:00 GMT+0200',
  apiURL: URL,
  mainMenuId: 2,
  mainPageId: 67,
  pageDataBaseURL: pageDataBaseURL,
  menuDataBaseURL: menuDataBaseURL,
  investmentURL: 'https://tokensale.crowdcoinage.com/api/sale/investments',
  leaderBoardURL: 'https://tokensale.crowdcoinage.com/api/campaign/leaderboard'
}
