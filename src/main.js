// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueHead from 'vue-head'
import VueCookies from 'vue-cookies'
import router from './router'
import VeeValidate, { Validator } from 'vee-validate'
import MultiLanguage from 'vue-multilanguage'
import { langMap } from '@/lang_map'
import { veeLang } from '@/vee_lang'
import VueMoment from 'vue-moment'
import VueCarousel from 'vue-carousel'
import VueScrollTo from 'vue-scrollto'
import VueClipboards from 'vue-clipboards'
import VTooltip from 'v-tooltip'

import Web3 from 'web3'

// Inject web3
let web3 = null
web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/6UxkpyBQ5ZBKCkory5SA'))

Vue.prototype.$web3 = web3

const languages = require('./languages')

Vue.use(MultiLanguage, languages)
Vue.use(VueHead)
Vue.use(VueCookies)
Vue.use(VeeValidate, veeLang)
Vue.use(VueMoment) // Used in: TokensaleSection.vue
Vue.use(VueCarousel) // Used in: TokensaleSection.vue
Vue.use(VTooltip)
Vue.use(VueScrollTo, {
  offset: -35,
  duration: 800,
  easing: 'ease'
})
Vue.use(VueClipboards)

Validator.extend('not_null', {
  getMessage: (field, params, data) => 'You must have purchased CCOS before applying for referral link',
  validate: (value, args) => value > 0
})

Vue.config.productionTip = false

// the main menu
new Vue({ // eslint-disable-line no-new
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  },
  created () {
    function setCookie (name, value, date) {
      var expires = ''
      if (date) {
        expires = '; expires=' + date.toUTCString()
      }
      document.cookie = name + '=' + value + expires + '; path=/'
    }

    // Set language
    this.language = window.language

    // Update lang attribute
    document.getElementsByTagName('html')[0].setAttribute('lang', this.language)

    // Set cookies
    setCookie('lang', this.language, new Date(2100, 3, 1))

    // Set correct IDs
    window.menuId = langMap[this.language].mainMenuId
    window.pageId = langMap[this.language][window.pageName]

    // Set Vee-Validate language
    Validator.localize(this.language)
  }
})
