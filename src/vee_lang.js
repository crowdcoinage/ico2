export var veeLang = {
  locale: 'en',
  dictionary: {
    en: {
      messages: {
        _default: (field) => 'This field is required',
        required: (field) => {
          switch (field) {
            case 'firstName':
              return 'First name is required'

            case 'lastName':
              return 'Last name is required'

            case 'email':
              return 'Email is required'

            case 'message':
              return 'Message is required'

            default:
              return 'This field is required'
          }
        },
        'not_null': 'You must have purchased CCOS before applying for referral link'
      },
      attributes: {}
    },
    ru: {
      messages: {
        _default: (field) => 'Необходимо заполнить графу',
        required: (field) => {
          switch (field) {
            case 'firstName':
              return 'Необходимо заполнить графу с именем'

            case 'lastName':
              return 'Необходимо заполнить графу с фамилией'

            case 'email':
              return 'Необходимо заполнить графу для имейла'

            case 'message':
              return 'Необходимо заполнить графу с сообщением'

            default:
              return 'Необходимо заполнить графу'
          }
        },
        'not_null': 'You must have purchased CCOS before applying for referral link'
      },
      attributes: {}
    },
    ch: {
      messages: {
        _default: (field) => 'This field is required',
        required: (field) => {
          switch (field) {
            case 'firstName':
              return 'First name is required'

            case 'lastName':
              return 'Last name is required'

            case 'email':
              return 'Email is required'

            case 'message':
              return 'Message is required'

            default:
              return 'This field is required'
          }
        },
        'not_null': 'You must have purchased CCOS before applying for referral link'
      },
      attributes: {}
    },
    kr: {
      messages: {
        _default: (field) => 'This field is required',
        'not_null': 'You must have purchased CCOS before applying for referral link'
      }
    },
    hi: {
      messages: {
        _default: (field) => 'This field is required',
        'not_null': 'You must have purchased CCOS before applying for referral link'
      }
    },
    jp: {
      messages: {
        _default: (field) => 'This field is required',
        'not_null': 'You must have purchased CCOS before applying for referral link'
      }
    },
    es: {
      messages: {
        _default: (field) => 'This field is required',
        'not_null': 'You must have purchased CCOS before applying for referral link'
      }
    },
    pt: {
      messages: {
        _default: (field) => 'This field is required',
        'not_null': 'You must have purchased CCOS before applying for referral link'
      }
    },
    vn: {
      messages: {
        _default: (field) => 'This field is required',
        'not_null': 'You must have purchased CCOS before applying for referral link'
      }
    }
  }
}
