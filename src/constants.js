export const BONUS_DATES = [
  {dateFrom: 'Feb 12, 2018 11:00:00 GMT+0200', dateTo: 'Feb 19, 2018 10:59:59 GMT+0200', percentage: 20, preSale: false, isFirst: true},
  {dateFrom: 'Feb 19, 2018 11:00:00 GMT+0200', dateTo: 'Feb 26, 2018 10:59:59 GMT+0200', percentage: 15, preSale: false, isFirst: false},
  {dateFrom: 'Feb 26, 2018 11:00:00 GMT+0200', dateTo: 'Mar 5, 2018 10:59:59 GMT+0200', percentage: 10, preSale: false, isFirst: false},
  {dateFrom: 'Mar 5, 2018 11:00:00 GMT+0200', dateTo: 'Mar 12, 2018 23:59:59 GMT+0200', percentage: 0, preSale: false, isFirst: false}
]

export const TOTAL_TOKENS = 39000000

export const START_DATE = 'Feb 12, 2018 11:00:00 GMT+0200'

export const END_DATE = 'Mar 12, 2018 23:59:59 GMT+0200'

export const REFERRALS = { // <-- add referral links here (uuid: contract address)
  '2fc151ed-2c62-4c50-bc4b-b7486b02c809': '0x2c9c5631fa9605855afbb167d8c35de0e3c88d07',
  '7d40c12b-48ec-482a-94bc-bb0d895b3357': '0x8fbd61280cb5272320a825c0afe67c1f032c0b0f',
  'f9cdc5f2-699a-487d-8a9f-24d708786b85': '0x43d6372b7fD20C6871244937B9D8A767aF14E1Ee',
  '12305509-cb59-4e37-950a-0b203df22b45': '0x27A2ECCb667Da50d948a9E8005F4C92712d795D0',
  '300b918a-a01c-44d4-a548-7f3ee23e9f4a': '0x04bf4553ed388187a1b58f9ca45d0589245c09ad',
  '4eecc40c-147d-4690-a89d-6b1db5d07276': '0xda3ce5118d719b8477a34ffd2d59ee722dc62841',
  '183dab65-9c1f-445e-afa5-425c0dc98067': '0x296bed6b6d55b51b6b25dda0cf19cf53f296c69a',
  'ede2803f-b941-4703-8ad8-d846a818ee42': '0x26e660171a98b000421bf4740cfa21a17df9b6c2',
  'e9daedf5-74c6-4628-b5d5-379f1db1a6ca': '0x6cc7163a78bbb64ec9b86fac871a0ccbdf036e7c',
  '3965a22e-ab34-4a1e-a53f-897323b14e6b': '0xa9e14cbb90388eb513505f7a09b07a3614ae4786',
  '06dc2669-c497-4ec4-a59f-77ff0ddd7665': '0x1b482a4fafd03b95af4ccb3cd020bf8b8595e984',
  '82435a69-547e-4eb2-aab0-5cf24e67359f': '0xd306f2940acf6419de81b9e523825186b5442e5f',
  'a084a7bd-14d5-427d-97ff-ee80bf3202f7': '0x636831B38028A5CcFf55daE522884a841Aa9620d',
  '77bf2d70-5ac6-459c-950c-c13dde90cbb4': '0xc6e472a4Dd026e7A9E4AD3C0ab92C06Da79605Aa',
  'db479f00-9ec0-4adc-8dff-a555b03a834f': '0x90B9a33b4CE3F68d9a091c85462A4b3Ce1E35F68',
  'e355e471-61a8-482d-b2c2-4cc2a89ab732': '0x1184032D3ab23d29982B085c7994956011b0CB29',
  '44e29f53-8ac3-453b-9396-e537754c9be3': '0x4d6906823bcc753f237783b65e3057bc62eb5a3c'
}

// https://crowdcoinage.com/crowdsale/__uuid__

export const CONTRACTS = {
  'crowdsale': {
    'address': '0x7f93bafc01ffa46d8735cc7f51914ac90cba061d',
    'abi': [
      {
        "constant": true,
        "inputs": [],
        "name": "weiRaised",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "endTime",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "tokensSold",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "tokensOnHold",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "tokensLeft",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "name": "customBonuses",
        "outputs": [
          {
            "name": "isReferral",
            "type": "bool"
          },
          {
            "name": "isSpecial",
            "type": "bool"
          },
          {
            "name": "referralAddress",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "name": "_weiAmount",
            "type": "uint256"
          }
        ],
        "name": "getBaseAmount",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "startTime",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "hasEnded",
        "outputs": [
          {
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "wallet",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "paused",
        "outputs": [
          {
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "name": "_wallet",
            "type": "address"
          }
        ],
        "name": "viewFunds",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "rate",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "token",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "destroy",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "payable": true,
        "stateMutability": "payable",
        "type": "fallback"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "unpause",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_wallet",
            "type": "address"
          }
        ],
        "name": "releaseFunds",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_startTime",
            "type": "uint256"
          }
        ],
        "name": "changeStartTime",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_contract",
            "type": "address"
          },
          {
            "name": "_isReferral",
            "type": "bool"
          },
          {
            "name": "_isSpecial",
            "type": "bool"
          },
          {
            "name": "_referralAddress",
            "type": "address"
          }
        ],
        "name": "setCustomBonus",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_endTime",
            "type": "uint256"
          }
        ],
        "name": "changeEndTime",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "pause",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_beneficiary",
            "type": "address"
          }
        ],
        "name": "buyTokens",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": true,
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_weiAmount",
            "type": "uint256"
          }
        ],
        "name": "hasEnoughTokensLeft",
        "outputs": [
          {
            "name": "",
            "type": "bool"
          }
        ],
        "payable": true,
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "addOnHold",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_amount",
            "type": "uint256"
          }
        ],
        "name": "subOnHold",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_beneficiary",
            "type": "address"
          },
          {
            "name": "_weiAmount",
            "type": "uint256"
          },
          {
            "name": "_tokensWithDecimals",
            "type": "uint256"
          }
        ],
        "name": "addOldInvestment",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "endSale",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "anonymous": false,
        "inputs": [],
        "name": "Unpause",
        "type": "event"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_wallet",
            "type": "address"
          }
        ],
        "name": "refundFunds",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_token",
            "type": "address"
          }
        ],
        "name": "setToken",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "name": "_startTime",
            "type": "uint256"
          },
          {
            "name": "_endTime",
            "type": "uint256"
          },
          {
            "name": "_rate",
            "type": "uint256"
          },
          {
            "name": "_wallet",
            "type": "address"
          },
          {
            "name": "_token",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [],
        "name": "Pause",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "purchaser",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "beneficiary",
            "type": "address"
          },
          {
            "indexed": false,
            "name": "value",
            "type": "uint256"
          },
          {
            "indexed": false,
            "name": "amount",
            "type": "uint256"
          }
        ],
        "name": "TokenPurchase",
        "type": "event"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_wallet",
            "type": "address"
          }
        ],
        "name": "setWallet",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_investment",
            "type": "uint256"
          }
        ],
        "name": "setMinInvestment",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "_recipient",
            "type": "address"
          }
        ],
        "name": "destroyAndSend",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ]
  },
  'special': {
    'address': '0x025fc8093a9679c743cc727478e250254cce5122'
  },
  'token': {
    'address': '0x79186ba0fc6fa49fd9db2f0ba34f36f8c24489c7'
  }
}

export const EARLYBIRD_NONCE = '658c424d-279f-4437-b412-ff873222fae0'

export const POSSIBLE_ENDINGS = [1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000, 10000000, 20000000]

export const LARGE_CONTR_BONUS = [
  {amount: 3, percentage: 10, level: 1},
  {amount: 5, percentage: 30, level: 2},
  {amount: 10, percentage: 50, level: 3},
  {amount: 30, percentage: 70, level: 4},
  {amount: 50, percentage: 80, level: 5}
]
