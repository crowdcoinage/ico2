import Vue from 'vue'
import { BONUS_DATES, START_DATE, END_DATE, CONTRACTS, EARLYBIRD_NONCE } from './constants'
import _ from 'lodash'

export const getCurrentBonus = () => {
  const date = new Date()

  for (const x in BONUS_DATES) {
    if (BONUS_DATES.hasOwnProperty(x)) {
      const i = BONUS_DATES[x]
      const start = new Date(i.dateFrom)
      const end = new Date(i.dateTo)

      if (date > start && date < end) {
        return i
      }
    }
  }

  return {
    percentage: 0,
    isFirst: false,
    dateTo: END_DATE
  }
}

export const getTokensSold = async () => {
  try {
    const web3 = Vue.prototype.$web3

    const crowdsaleInstance = new web3.eth.Contract(CONTRACTS['crowdsale']['abi'], CONTRACTS['crowdsale']['address'])

    const tokensSold = await crowdsaleInstance.methods.tokensSold().call()
    return web3.utils.fromWei(tokensSold)
  } catch (e) {
    console.log(e)
    return 0
  }
}

export const isLive = () => {
  // return true

  const start = new Date(START_DATE)
  const current = new Date()

  return start <= current
}

export const formatNumbers = (value, fixed = 2) => {
  if (!_.isFinite(parseFloat(value))) {
    return value
  }

  const p = Number(value).toFixed(fixed).split('.')

  return p[0].split('').reverse().reduce((acc, value, i, orig) => {
    return value === '-' ? acc : value + (i && !(i % 3) ? ',' : '') + acc
  }, '') + (p[1] ? '.' + p[1] : '')
}
