# crowdcoinage

> OS for the future of funding

## Build Setup

``` bash
# install dependencies
npm install
npm install --save axios jquery
yarn add swiper@3.4.2
npm install --save-dev babel-polyfill

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
# (make sure to update app.css version in index.html)
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
