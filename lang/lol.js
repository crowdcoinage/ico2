const json2csv = require('fs');
var parseXlsx = require('excel');
var _ = require('lodash');

var en = require('../src/languages').en
const LANG = 'pt'

parseXlsx(LANG+'.xlsx', function(err, data) {
  if(err) throw err;

  var translated = {}

  for (var i = 0; i <= data.length-1; i++) {
    if (data[i][1] != '') {
      for (var x in en) {
        if (data[i][0].toLowerCase() == en[x].toLowerCase()) {
          translated[x] = data[i][1]
          break
        }
      }
    }
  }

  var endClone = _.clone(en)

  translated = Object.assign({}, endClone, translated);

  fs.writeFile(LANG+'.csv', csv, function(err) {
    if (err) throw err;
    console.log(LANG+'.csv saved');
  });
});