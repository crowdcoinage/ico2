const json2csv = require('json2csv');
const fs = require('fs');

var langs = ['ru', 'ch', 'jp', 'es', 'pt', 'vn']

var en = require('../src/languages').en

var fields = ['key', 'english', 'translation']
var data = []

for (var LANG of langs) {
  var foreign = require('../src/languages')[LANG]

  for (var x in foreign) {
    if (foreign[x].toLowerCase() == en[x].toLowerCase()) {
      data.push({'key': x, 'english': en[x], 'foreign': ''})
    }
  }

  var csv = json2csv({ data, fields });

  fs.writeFile(LANG+'_untranslated.csv', csv, function(err) {
    if (err) throw err;
    console.log('file saved');
  });
}

